---
title: "exploration-donees"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{exploration-donees}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)

pkgload::load_all()
```

```{r setup}
rds <- list.files(pattern = "rds$", path = system.file("data-origin", package = "propre.itdd"), full.names = TRUE)

datas <- lapply(rds, function(x){readr::read_rds(x)})
```
