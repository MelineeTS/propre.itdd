---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# propre.itdd

Antidote

# Installation 

```{r, eval=FALSE}
remotes::install_gitlab('rdes_dreal/propre.itdd')
```


# Application :

<https://connect.thinkr.fr/antidote/>